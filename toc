#! /usr/bin/env python3

""" Reads the files in current directory and creates a table-of-contents output
to be included in the README.md.
"""

__author__ = 'vivekrai'
__version__ = '1.0'

import os
import argparse
import sys
from collections import defaultdict
import operator


__ignore = ['./.git', '.pyc', '._DS_Store']
__categories = {
        'misc': 'Miscellaneous',
        'python': 'Python (lang)',
        'R': 'R (lang)',
        'unix': 'GNU Unix | Command line',
        'algorithms': 'Algorithms',
        'productivity': 'Productivity',
        'computing': 'Computing',
        'writing': 'Writing',
        'math': 'Statistics | Methods | Math',
        'biology': 'Biology',
        'lessons': 'Zen lessons',
        'unclassified': 'Unclassified'
        }


def scan_files(path):
    tree = defaultdict(list)
    for root, dirs, files in os.walk(path):
        if root in __ignore:  # ignore blacklist
            dirs[:], files[:] = [], []
        elif root == '.':  # skip root
            continue
        else:
            tree[root] = files
    return tree


def get_title(filepath):
    with open(filepath, 'r') as f:
        title = f.readline()
    return title.strip('#').strip()


def get_permalink(title):
    fix_pipes = '--'.join([x.strip() for x in title.lower().split('|')])
    return '-'.join(fix_pipes.split(' '))


def write_entries(root, output):
    files = scan_files(root)
    out = []

    out.append("**Count: {}**\n\n".format(sum([len(x) for x in files.values()])))

    # write short-toc
    out.append("## Contents\n")
    for k, v in sorted(files.items(), key=operator.itemgetter(0)):
        cleaned_title = __categories[k.strip('./')]
        out.append("* [{}](#{})\n".format(cleaned_title,
                                          get_permalink(cleaned_title)))
    out.append("\n")


    # write full-toc
    for k, v in sorted(files.items(), key=operator.itemgetter(0)):
        out.append("### {}\n".format(__categories[k.strip('./')]))
        for file_ in v:
            path_ = os.path.join(k, file_)
            out.append("* [{}]({})\n".format(get_title(path_), path_))
        out.append("\n")

    output = sys.stdout if not output else open(output, 'w')

    with output as f:
        f.writelines(out)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="""
    Prints a markdown listing of posts from the til directory to target file
    (default: stdout). You may use the output whicever way you like.""")

    parser.add_argument('-r', '--root',
                        help="Root directory to scan for files",
                        default='.')

    parser.add_argument('-o', '--output',
                        help="Output file (default: stdout)")

    args = parser.parse_args()

    write_entries(args.root, args.output)
