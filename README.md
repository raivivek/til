# TIL (Today I Learned)

A collection of small snippets that I learn day to day across a variety of tasks
I am involved in. Often, they are curious and useful insights that I wish to
share with everyone but are too small for a full blog post.

**Count: 40**

## Contents
* [R (lang)](#r-(lang))
* [Algorithms](#algorithms)
* [Biology](#biology)
* [Computing](#computing)
* [Zen lessons](#zen-lessons)
* [Statistics | Methods | Math](#statistics--methods--math)
* [Miscellaneous](#miscellaneous)
* [Productivity](#productivity)
* [Python (lang)](#python-lang)
* [Unclassified](#unclassified)
* [GNU Unix | Command line](#gnu-unix--command-line)
* [Writing](#writing)

### R (lang)
* [Large datasets in `R`](./R/r-large-data.md)
* [Modifying `ggplot2` objects after creation](./R/modifying-ggplot2-objects-after-creation.md)
* [Using-conditionals-(`if`-`else`)-in-data-pipes.md](./R/using-conditionals-(`if`-`else`)-in-data-pipes.md)
* [Using Microsoft R Open (MRO)](./R/using-microsoft-r-open-3.5.x.md)

### Algorithms
* [Newick Tree Format](./algorithms/newick-tree-format.md)
* [KMP Algorithm](./algorithms/kmp-matcher.md)

### Biology
* [Pedigree Collapse](./biology/pedigree-collapse.md)
* [The Omnigenic Model](./biology/the-omnigenic-model.md)
* ['Thrifty' gene hypothesis](./biology/'thrifty'-gene-hypothesis.md)

### Computing
* [Snakemake on HPC](./computing/snakemake-on-hpc.md)
* [SOAP-vs-REST stack](./computing/SOAP-vs-REST.md)
* [Database comparison](./computing/databases.md)
* [Editing remote files](./computing/editing-remote-file.md)
* [`pkg-config` for compilation](./computing/pkg-config-for-compilation.md)

### Zen lessons
* [The value of being slow](./lessons/the-value-of-being-slow.md)

### Statistics | Methods | Math
* [Permutation testing](./math/permutation-testing.md)

### Miscellaneous
* [Banzhaf Power Index](./misc/banzhaf-index.md)
* [HMMER](./misc/hmmer.md)
* [Choosing right Wifi channel](./misc/choosing-wifi-channel.md)
* [Using Zotero with WebDAV for file storage](./misc/zotero-webdav-setup.md)
* [Pizza Effect](./misc/pizza-effect.md)
* [Flash Asus TM-AC1900 to RT-AC68U](https://gist.github.com/raivivek/09b515420735a259091fc9e20d55dcb2)
* [Using `cookiecutter` for project skeletons](./misc/cookiecutter-skeleton.md)

### Productivity
* [Getting Research Idea I](./productivity/getting-research-idea-i.md)
* [Quickly organizing ideas](./productivity/quickly-organizing-ideas.md)

### Python (lang)
* [`itertools` module](./python/itertools-module.md)
* [ORMs and Database migration](./python/sql-orm.md)
* [Records: SQL for Humans](./python/records-sql.md)
* [pymust.watch](./python/pymust-watch.md)
* [Don't use `f.readlines()`](./python/dont-use-readlines.md)
* [Super considered super!](./python/super-talk.md)
* [Writing Conda Recipes](./python/conda-recipes.md)
* [`contextlib` module](./python/contextlib-with.md)
* [`multiprocessing` module in Python](./python/optimize-pandas-mp.md)
* [`pandas.get_dummies`: Vectorize category variables](./python/pandas-get-dummies.md)
* [Care with `string.split()`](./python/care-with-string-split.md)
* [Buffered vs Unbuffered I/O in Python](./python/unbuffered-output.md)
* [Check if `STDIN` is `tty` or piped](./python/checking-if-STDIN-is-tty.md)

### GNU Unix | Command line
* [Fork a command to background](./unix/fork-to-bg.md)
* [Fetch random lines from file](./unix/random-lines.md)
* [Concatenate files with same header](./unix/concatenate-files-with-same-header.md)
* [Watch LaTeX file and autocompile](./unix/watch-compile-latex.md)
* [Changing priority of running programs](./unix/ionicing-programs.md)
* [OOM Killer](./unix/oom-killer.md)
* [Run a command for some time](./unix/timeout.md)

### Writing
* [Headlines and the Inverted Pyramid](./writing/inverted-pyramid.md)
* [Double Entry Journal](./writing/double-entry-journal.md)
* [Rhetorical Précis Format](./writing/rhetorical-precis-format.md)

---

## Other TIL Collections

* [Today I Learned by Hashrocket](https://til.hashrocket.com)
* [jwworth/til](https://github.com/jwworth/til)
* [thoughtbot/til](https://github.com/thoughtbot/til)

## License

© 2015-2018 Vivek Rai
